package br.edu.infnet.isat.report;

import br.edu.infnet.isat.entity.*;
import br.edu.infnet.isat.entity.Class;
import br.edu.infnet.isat.service.FormService;
import br.edu.infnet.isat.service.SurveyService;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Component
public class SurveyReport {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyReport.class);

    @Autowired
    private FormService formService;

    @Autowired
    private SurveyService surveyService;

    public void create(Survey survey) throws IOException {
        Workbook wb = new XSSFWorkbook();
        CreationHelper createHelper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet("Plan1");
        for (Class clazz : survey.getClasses()) {
            //TURMA
            Row row = sheet.createRow(0);
            row.createCell(0).setCellValue(createHelper.createRichTextString("Turma:"));
            row.createCell(1).setCellValue(createHelper.createRichTextString(clazz.getName()));

            //PESQUISA
            Row row1 = sheet.createRow(1);
            row1.createCell(0).setCellValue(createHelper.createRichTextString("Pesquisa:"));
            row1.createCell(1).setCellValue(createHelper.createRichTextString(survey.getName()));

            //QUESTÔES
            Row row3 = sheet.createRow(3);
            row3.createCell(0);

            List<Question> questions = survey.getQuestions();

            for (int i = 0; i < questions.size(); i++) {
                row3.createCell(i + 1).setCellValue(createHelper.createRichTextString(questions.get(i).getDescription()));
            }

            row3.createCell(row3.getLastCellNum()).setCellValue("Média");

            List<Form> forms = formService.findBySurveyAndClass(survey.getId(), clazz.getId());

            for (int i = 0; i < forms.size(); i++) {
                Form form = forms.get(i);
                Row rowN = sheet.createRow(i + 4);
                rowN.createCell(0).setCellValue(createHelper.createRichTextString(form.getStudent().getName()));

                for (int j = 0; j < form.getAnswers().size(); j++) {
                    Answer answer = form.getAnswers().get(j);
                    Integer value = answer.getRate() >= 0 ? answer.getRate() : 0;
                    rowN.createCell(j + 1).setCellValue(value);
                }

                String firstColumn = CellReference.convertNumToColString(1);
                String lastColumn = CellReference.convertNumToColString(rowN.getLastCellNum() - 1);

                rowN.createCell(rowN.getLastCellNum()).setCellFormula(String.format("AVERAGE(%s%s:%s%s)", firstColumn, rowN.getRowNum() + 1, lastColumn, rowN.getRowNum() + 1));
            }

            int lastRowNum = sheet.getLastRowNum();
            Row lastRow = sheet.getRow(lastRowNum);
            int lastCellNum = lastRow.getLastCellNum() - 1;
            String averageColumn = CellReference.convertNumToColString(lastCellNum);

            sheet.createRow(lastRowNum + 1).createCell(lastCellNum).setCellFormula(String.format("AVERAGE(%s%s:%s%s)", averageColumn, 5, averageColumn, lastRow.getRowNum() + 1));

            String name = survey.getName().replaceAll(" ", "").toLowerCase() + "-" + clazz.getName().replaceAll(" ", "").toLowerCase() + ".xlsx";

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(name);
            wb.write(fileOut);
            fileOut.close();

            surveyService.updateReported(survey);

            LOGGER.debug("Report [{}] was generated successfully", name);
        }
    }

}
