package br.edu.infnet.isat.resource;

import br.edu.infnet.isat.entity.Question;
import br.edu.infnet.isat.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api")
public class ApiController {

    @Autowired
    private QuestionService questionService;

    @GetMapping("question")
    public List<Question> findByCategories(@RequestParam List<Long> categories) {
        return questionService.findByCategories(categories);
    }

}
