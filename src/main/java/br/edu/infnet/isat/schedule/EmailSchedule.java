package br.edu.infnet.isat.schedule;

import br.edu.infnet.isat.entity.Class;
import br.edu.infnet.isat.entity.Form;
import br.edu.infnet.isat.entity.Student;
import br.edu.infnet.isat.entity.Survey;
import br.edu.infnet.isat.service.FormService;
import br.edu.infnet.isat.service.SurveyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@Component
public class EmailSchedule {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSchedule.class);

    @Value("${domain}")
    private String domain;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Scheduled(cron = "0/30 * * * * *")
    public void sendEmail() throws MessagingException {
        LocalDate now = LocalDate.now();
        Instant instant = now.atStartOfDay(ZoneId.systemDefault()).toInstant();

        Date today = Date.from(instant);
        Date tomorrow = Date.from(instant.plus(1, ChronoUnit.DAYS));

        List<Survey> surveys = surveyService.findByStartDateBetweenAndFired(today, tomorrow, false);

        for (Survey survey : surveys) {
            List<Class> classes = survey.getClasses();

            for (Class clazz : classes) {
                List<Student> students = clazz.getStudents();

                for (Student student : students) {
                    Form form = formService.save(clazz, student, survey);

                    if (form != null) {
                        String to = student.getEmail();
                        String subject = survey.getName();
                        String link = domain + "/form/" + form.getCode();

                        Context context = new Context();
                        context.setVariable("name", student.getName());
                        context.setVariable("link", link);

                        String htmlContent = templateEngine.process("mail/template", context);

                        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                        mimeMessageHelper.setSubject(subject);
                        mimeMessageHelper.setTo(to);
                        mimeMessageHelper.setText(htmlContent, true);

                        javaMailSender.send(mimeMessage);
                        LOGGER.debug("Email with subject [{}] was sent to [{}]", subject, to);
                    }
                }
            }

            surveyService.updateFired(survey);
        }
    }

}
