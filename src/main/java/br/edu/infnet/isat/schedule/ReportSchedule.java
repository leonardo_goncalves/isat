package br.edu.infnet.isat.schedule;

import br.edu.infnet.isat.entity.Survey;
import br.edu.infnet.isat.report.SurveyReport;
import br.edu.infnet.isat.service.SurveyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Component
public class ReportSchedule {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportSchedule.class);

    @Autowired
    private SurveyReport report;

    @Autowired
    private SurveyService surveyService;

    @Scheduled(cron = "0/30 * * * * *")
    public void generateReport() {
        try {
            LocalDate now = LocalDate.now();
            Instant instant = now.atStartOfDay(ZoneId.systemDefault()).toInstant();

            Date today = java.util.Date.from(instant);

            List<Survey> surveys = surveyService.findByEndDateBeforeAndReported(today, false);

            for (Survey survey : surveys) {
                report.create(survey);
            }
        } catch (IOException e) {
            LOGGER.error("Error when generating report");
        }
    }

}
