package br.edu.infnet.isat.enums;

import org.apache.commons.lang3.builder.ToStringBuilder;

public enum Color {

    RED(0, "red"),
    ORANGE(1, "orange"),
    YELLOW(2, "yellow"),
    OLIVE(3, "olive"),
    GREEN(4, "green"),
    TEAL(5, "teal"),
    BLUE(6, "blue"),
    VIOLET(7, "violet"),
    PURPLE(8, "purple"),
    PINK(9, "pink"),
    BROWN(10, "brown"),
    GREY(11, "grey"),
    BLACK(12, "black");

    private final int value;
    private final String name;

    Color(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static Color valueOf(int value) {
        for (Color color: Color.values()) {
            if(color.value == value) {
                return color;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("value", value)
            .append("name", name)
            .toString();
    }

}
