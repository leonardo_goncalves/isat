package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Category;
import br.edu.infnet.isat.enums.Color;
import br.edu.infnet.isat.repository.CategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class CategoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);

    @Autowired
    private CategoryRepository repository;

    public List<Category> findAll() {
        return repository.findAll();
    }

    public Category findOne(Long id) {
        return repository.findOne(id);
    }

    public Category save(Category category) {
        Random random = new Random();
        category.setColor(Color.valueOf(random.nextInt(12)));
        category.setName(category.getName().toLowerCase());
        return repository.save(category);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
