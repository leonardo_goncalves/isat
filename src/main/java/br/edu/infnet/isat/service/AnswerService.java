package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Answer;
import br.edu.infnet.isat.repository.AnswerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnswerService.class);

    @Autowired
    private AnswerRepository repository;

    public List<Answer> findAll() {
        return repository.findAll();
    }

    public Answer findOne(Long id) {
        return repository.findOne(id);
    }

    public Answer save(Answer answer) {
        return repository.save(answer);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
