package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.*;
import br.edu.infnet.isat.entity.Class;
import br.edu.infnet.isat.repository.AnswerRepository;
import br.edu.infnet.isat.repository.FormRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class FormService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormService.class);

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private AnswerRepository answerRepository;

    public List<Form> findAll() {
        return formRepository.findAll();
    }

    public Form findOne(Long id) {
        return formRepository.findOne(id);
    }

    public Form save(Form form) {
        return formRepository.save(form);
    }

    public void delete(Long id) {
        formRepository.delete(id);
    }

    public void deleteAll() {
        formRepository.deleteAll();
    }

    public Form save(Class clazz, Student student, Survey survey) {
        Form form = new Form(clazz, student, survey);
        form.setCode(UUID.randomUUID().toString());
        return formRepository.save(form);
    }

    public Form findByCode(String code) {
        return formRepository.findByCode(code);
    }

    public Form updateWithAnswers(Form form) {
        if (!form.getAnswers().isEmpty()) {
            Form persistedForm = formRepository.findByCode(form.getCode());
            if (persistedForm != null && persistedForm.getId() != null) {
                Survey survey = persistedForm.getSurvey();
                List<Question> questions = survey.getQuestions();
                List<Answer> answers = form.getAnswers();
                for (int i = 0; i < answers.size(); i++) {
                    Answer answer = answers.get(i);
                    answer.setQuestion(questions.get(i));
                    answer.setForm(persistedForm);
                    answerRepository.save(answer);
                }
                persistedForm.setSubmitDate(new Date());
                return formRepository.save(persistedForm);
            }
        }
        return null;
    }

    public List<Form> findBySurvey(Long survey) {
        return formRepository.findBySurvey(survey);
    }

    public List<Form> findBySurveyAndClass(Long survey, Long clazz) {
        return formRepository.findBySurveyAndClass(survey, clazz);
    }
}
