package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Module;
import br.edu.infnet.isat.repository.ModuleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleService.class);

    @Autowired
    private ModuleRepository repository;

    public List<Module> findAll() {
        return repository.findAll();
    }

    public Module findOne(Long id) {
        return repository.findOne(id);
    }

    public Module save(Module module) {
        return repository.save(module);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
