package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Survey;
import br.edu.infnet.isat.repository.SurveyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SurveyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyService.class);

    @Autowired
    private SurveyRepository repository;

    public List<Survey> findAll() {
        return repository.findByOrderByCreateDateDesc();
    }

    public Survey findOne(Long id) {
        return repository.findOne(id);
    }

    public Survey save(Survey survey) {
        if (survey.getCreateDate() == null) {
            survey.setCreateDate(new Date());
            survey.setFired(false);
        }
        return repository.save(survey);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public List<Survey> findByStartDateBetweenAndFired(Date before, Date after, Boolean fired) {
        return repository.findByStartDateBetweenAndFired(before, after, fired);
    }

    public List<Survey> findByEndDateBeforeAndReported(Date date, Boolean reported) {
        return repository.findByEndDateBeforeAndReported(date, reported);
    }

    public Survey updateFired(Survey survey) {
        survey.setFired(true);
        return repository.save(survey);
    }

    public Survey updateReported(Survey survey) {
        survey.setReported(true);
        return repository.save(survey);
    }

}
