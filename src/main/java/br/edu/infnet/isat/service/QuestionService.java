package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Question;
import br.edu.infnet.isat.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private QuestionRepository repository;

    public List<Question> findAll() {
        return repository.findAll();
    }

    public Question findOne(Long id) {
        return repository.findOne(id);
    }

    public Question save(Question question) {
        return repository.save(question);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public List<Question> findByCategories(List<Long> categories) {
        return repository.findByCategories(categories);
    }

}
