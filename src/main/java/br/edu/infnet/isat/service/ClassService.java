package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Class;
import br.edu.infnet.isat.repository.ClassRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassService.class);

    @Autowired
    private ClassRepository repository;

    public List<Class> findAll() {
        return repository.findAll();
    }

    public Class findOne(Long id) {
        return repository.findOne(id);
    }

    public Class save(Class clazz) {
        return repository.save(clazz);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
