package br.edu.infnet.isat.service;

import br.edu.infnet.isat.entity.Teacher;
import br.edu.infnet.isat.repository.TeacherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherService.class);

    @Autowired
    private TeacherRepository repository;

    public List<Teacher> findAll() {
        return repository.findAll();
    }

    public Teacher findOne(Long id) {
        return repository.findOne(id);
    }

    public Teacher save(Teacher teacher) {
        return repository.save(teacher);
    }

    public void delete(Long id) {
        repository.delete(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}
