package br.edu.infnet.isat.controller;

import br.edu.infnet.isat.entity.Question;
import br.edu.infnet.isat.service.CategoryService;
import br.edu.infnet.isat.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute(questionService.findAll());
        return "questions";
    }

    @GetMapping("/add")
    public String add(@ModelAttribute Question question, Model model) {
        model.addAttribute(categoryService.findAll());
        return "question-form";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable Long id) {
        model.addAttribute(categoryService.findAll());
        model.addAttribute(questionService.findOne(id));
        return "question-form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        questionService.delete(id);
        return "redirect:/question";
    }

    @PostMapping("/save")
    public String save(@Valid Question question, BindingResult result) {
        if (result.hasErrors()) {
            return "question-form";
        }
        questionService.save(question);
        return "redirect:/question";
    }

}
