package br.edu.infnet.isat.controller;

import br.edu.infnet.isat.entity.Survey;
import br.edu.infnet.isat.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("survey")
public class SurveyController {

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private ClassService classService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private UserService userService;

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute(surveyService.findAll());
        return "surveys";
    }

    @GetMapping("/add")
    public String add(@ModelAttribute Survey survey, Model model) {
        model.addAttribute(classService.findAll());
        model.addAttribute(categoryService.findAll());
        model.addAttribute(questionService.findAll());
        return "survey-form";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable Long id) {
        model.addAttribute(categoryService.findAll());
        model.addAttribute(classService.findAll());
        model.addAttribute(questionService.findAll());
        model.addAttribute(surveyService.findOne(id));
        return "survey-form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        surveyService.delete(id);
        return "redirect:/survey";
    }

    @PostMapping("/save")
    public String save(@Valid Survey survey, BindingResult result, Authentication authentication) {
        if (result.hasErrors()) {
            return "survey-form";
        }
//        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
//        User user = userService.findByUsername(userDetails.getUsername());
//        survey.setUser(user);
        surveyService.save(survey);
        return "redirect:/survey";
    }

}
