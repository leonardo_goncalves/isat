package br.edu.infnet.isat.controller;

import br.edu.infnet.isat.entity.Form;
import br.edu.infnet.isat.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Date;

@Controller
@RequestMapping("form")
public class FormController {

    @Autowired
    private FormService formService;

    @GetMapping("{code}")
    public String findOne(Model model, @PathVariable String code) {
        Form form = formService.findByCode(code);
        if (form != null) {
            Date endDate = form.getSurvey().getEndDate();
            if (endDate.before(Date.from(Instant.now()))) {
                model.addAttribute("message", "O período para responder essa pesquisa terminou.");
            }
            if (form.getSubmitDate() == null) {
                model.addAttribute(form);
                return "form";
            } else {
                model.addAttribute("message", "Você já respondeu essa pesquisa anteriormente");
            }
        } else {
            model.addAttribute("message", "A pesquisa solicitada não pode ser encontrada");
        }
        return "feedback";
    }

    @PostMapping
    public String submitForm(@Valid Form form, Model model) {
        formService.updateWithAnswers(form);
        model.addAttribute("message", "Parabéns!!! Você respondeu a pesquisa com sucesso!");
        return "feedback";
    }

}
