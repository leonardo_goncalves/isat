package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Query("select q from Question q join fetch q.categories c where c.id in ?1")
    List<Question> findByCategories(List<Long> categories);

}
