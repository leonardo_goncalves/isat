package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
