package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
