package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SurveyRepository extends JpaRepository<Survey, Long> {

    List<Survey> findByOrderByCreateDateDesc();

    List<Survey> findByStartDateBetweenAndFired(Date before, Date after, Boolean fired);

    List<Survey> findByEndDateBeforeAndReported(Date date, Boolean reported);

}
