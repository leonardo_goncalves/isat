package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Class;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepository extends JpaRepository<Class, Long> {
}
