package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
