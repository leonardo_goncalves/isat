package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
