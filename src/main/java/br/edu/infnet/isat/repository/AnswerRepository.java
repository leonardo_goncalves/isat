package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
