package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Form;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FormRepository extends JpaRepository<Form, Long> {

    Form findByCode(String code);

    @Query("select f from Form f join fetch f.survey s where s.id = ?1")
    List<Form> findBySurvey(Long survey);

    @Query("select f from Form f join fetch f.survey s join fetch f.clazz c where s.id = ?1 and c.id = ?2")
    List<Form> findBySurveyAndClass(Long survey, Long clazz);

}
