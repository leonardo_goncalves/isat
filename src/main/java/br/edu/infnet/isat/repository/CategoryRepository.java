package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
