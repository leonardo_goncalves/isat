package br.edu.infnet.isat.repository;

import br.edu.infnet.isat.entity.Module;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuleRepository extends JpaRepository<Module, Long> {
}
