insert into user (id, name, username, password, enabled) values (1, 'Administrator', 'admin', 'admin', true);

insert into role (id, name) values (1, 'ADMIN');

insert into user_role (role_id, user_id) values (1, 1);

insert into category(id, name, color) values (1, 'geral', 1);
insert into category(id, name, color) values (2, 'professor', 2);
insert into category(id, name, color) values (3, 'infraestrutura', 3);
insert into category(id, name, color) values (4, 'conteúdo', 4);

insert into question(id, description) values (1, 'Até agora, o curso está atingindo as minhas expectativas.');
insert into question(id, description) values (2, 'Até agora, eu indicaria o curso para um amigo.');
insert into question(id, description) values (3, 'Até agora, o curso me parece voltado para as necessidades do mercado.');
insert into question(id, description) values (4, 'Até agora, a coordenação pedagógica parece comprometida com a qualidade do curso.');
insert into question(id, description) values (5, 'Até agora, minha turma parece proporcionar um networking relevante para a minha carreira.');
insert into question(id, description) values (6, 'Até agora, o atendimento de Secretaria que recebi está atingindo as minhas expectativas.');

insert into question_category(category_id, question_id) values (1, 1);
insert into question_category(category_id, question_id) values (1, 2);
insert into question_category(category_id, question_id) values (1, 3);
insert into question_category(category_id, question_id) values (1, 4);
insert into question_category(category_id, question_id) values (1, 5);
insert into question_category(category_id, question_id) values (1, 6);

insert into class(id, name) values (1, 'Turma 1');
insert into class(id, name) values (2, 'Turma 2');
insert into class(id, name) values (3, 'Turma 3');
insert into class(id, name) values (4, 'Turma 4');

insert into student(id, registry, name, email) values (1, 171010001, 'Cristiano', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (2, 171010002, 'Maria', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (3, 171010003, 'Jose', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (4, 171010004, 'Ana', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (5, 171010005, 'Douglas', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (6, 171010006, 'Pedro', 'crisvinte2@yahoo.com');
insert into student(id, registry, name, email) values (7, 171010007, 'Julia', 'crisvinte2@yahoo.com');

insert into student_class(class_id, student_id) values(1, 1);
insert into student_class(class_id, student_id) values(2, 2);
insert into student_class(class_id, student_id) values(2, 3);
insert into student_class(class_id, student_id) values(4, 5);
insert into student_class(class_id, student_id) values(4, 6);
insert into student_class(class_id, student_id) values(4, 7);

insert into survey(id, name, create_date, start_date, end_date, fired, reported) values(1, 'Pesquisa 01', '2017-11-26', '2017-11-26', '2017-11-27', true, false);

insert into survey_category(survey_id, category_id) values(1, 1);

insert into survey_question(survey_id, question_id) values(1, 1);
insert into survey_question(survey_id, question_id) values(1, 2);
insert into survey_question(survey_id, question_id) values(1, 3);
insert into survey_question(survey_id, question_id) values(1, 4);
insert into survey_question(survey_id, question_id) values(1, 5);
insert into survey_question(survey_id, question_id) values(1, 6);

insert into survey_class(class_id, survey_id) values(1, 1);
insert into survey_class(class_id, survey_id) values(2, 1);
insert into survey_class(class_id, survey_id) values(4, 1);

insert into form(id, code, class_id, student_id, survey_id, submit_date) values (1, 'd30831c5-ec53-4b31-9603-176014318f5a', 1, 1, 1, '2017-11-26');
insert into form(id, code, class_id, student_id, survey_id, submit_date) values (2, 'd5508b06-d3ac-4255-aaf0-075684841e66', 2, 2, 1, '2017-11-26');
insert into form(id, code, class_id, student_id, survey_id, submit_date) values (3, 'af76e3e2-eead-4f03-b322-13bcc97f551c', 2, 3, 1, '2017-11-26');
insert into form(id, code, class_id, student_id, survey_id, submit_date) values (4, '91b3f5be-d14b-11e7-8941-cec278b6b50a', 4, 5, 1, '2017-11-26');
insert into form(id, code, class_id, student_id, survey_id, submit_date) values (5, '91b3fa0a-d14b-11e7-8941-cec278b6b50a', 4, 6, 1, '2017-11-26');
insert into form(id, code, class_id, student_id, survey_id, submit_date) values (6, '91b3fec4-d14b-11e7-8941-cec278b6b50a', 4, 1, 1, '2017-11-26');

insert into answer(id, rate, form_id, question_id) values(1, 3, 1, 1);
insert into answer(id, rate, form_id, question_id) values(2, 3, 1, 2);
insert into answer(id, rate, form_id, question_id) values(3, 3, 1, 3);
insert into answer(id, rate, form_id, question_id) values(4, 3, 1, 4);
insert into answer(id, rate, form_id, question_id) values(5, 3, 1, 5);
insert into answer(id, rate, form_id, question_id) values(6, 3, 1, 6);

insert into answer(id, rate, form_id, question_id) values(7, 1, 2, 1);
insert into answer(id, rate, form_id, question_id) values(8, 2, 2, 2);
insert into answer(id, rate, form_id, question_id) values(9, 3, 2, 3);
insert into answer(id, rate, form_id, question_id) values(10, 4, 2, 4);
insert into answer(id, rate, form_id, question_id) values(11, 5, 2, 5);
insert into answer(id, rate, form_id, question_id) values(12, 0, 2, 6);

insert into answer(id, rate, form_id, question_id) values(13, 1, 3, 1);
insert into answer(id, rate, form_id, question_id) values(14, 1, 3, 2);
insert into answer(id, rate, form_id, question_id) values(15, 1, 3, 3);
insert into answer(id, rate, form_id, question_id) values(16, 1, 3, 4);
insert into answer(id, rate, form_id, question_id) values(17, 1, 3, 5);
insert into answer(id, rate, form_id, question_id) values(18, 1, 3, 6);

insert into answer(id, rate, form_id, question_id) values(19, 2, 4, 1);
insert into answer(id, rate, form_id, question_id) values(20, 2, 4, 2);
insert into answer(id, rate, form_id, question_id) values(21, 3, 4, 3);
insert into answer(id, rate, form_id, question_id) values(22, 3, 4, 4);
insert into answer(id, rate, form_id, question_id) values(23, 2, 4, 5);
insert into answer(id, rate, form_id, question_id) values(24, 4, 4, 6);

insert into answer(id, rate, form_id, question_id) values(25, 3, 5, 1);
insert into answer(id, rate, form_id, question_id) values(26, 2, 5, 2);
insert into answer(id, rate, form_id, question_id) values(27, 3, 5, 3);
insert into answer(id, rate, form_id, question_id) values(28, 5, 5, 4);
insert into answer(id, rate, form_id, question_id) values(29, 0, 5, 5);
insert into answer(id, rate, form_id, question_id) values(30, 1, 5, 6);

insert into answer(id, rate, form_id, question_id) values(31, 1, 6, 1);
insert into answer(id, rate, form_id, question_id) values(32, 2, 6, 2);
insert into answer(id, rate, form_id, question_id) values(33, 4, 6, 3);
insert into answer(id, rate, form_id, question_id) values(34, 5, 6, 4);
insert into answer(id, rate, form_id, question_id) values(35, 0, 6, 5);
insert into answer(id, rate, form_id, question_id) values(36, 1, 6, 6);
