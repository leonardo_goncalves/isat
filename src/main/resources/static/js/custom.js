$('.message .close').on('click', function () {
    $(this).closest('.message').transition('fade');
});

$('.ui.dropdown').dropdown({
    allowAdditions: true
});

$('.ui.form').form({
    fields: {
        username: {
            identifier: 'username',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Por favor entre com seu {name}'
                }
            ]
        },
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Por favor entre com a sua {name}'
                },
                {
                    type: 'length[5]',
                    prompt: 'Sua {name} precisa ter no mínimo 5 caracteres'
                }
            ]
        },
        name: {
            identifier: 'name',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        startDate: {
            identifier: 'startDate',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        endDate: {
            identifier: 'endDate',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        categories: {
            identifier: 'categories',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        questions: {
            identifier: 'questions',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        'classes': {
            identifier: 'classes',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        },
        description: {
            identifier: 'description',
            rules: [
                {
                    type: 'empty',
                    prompt: '{name} deve ser preenchido.'
                }
            ]
        }
    }
});

$(document).ready(function(){
    $('#startDate').datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        minDate: new Date(),
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });

    $('#endDate').datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        minDate: new Date(),
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
});
